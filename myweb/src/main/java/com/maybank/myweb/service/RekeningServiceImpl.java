package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.Provider;
import com.maybank.myweb.entity.Rekening;
import com.maybank.myweb.repository.NasabahRepo;
import com.maybank.myweb.repository.ProviderRepo;
import com.maybank.myweb.repository.RekeningRepo;


@Service
@Transactional
public class RekeningServiceImpl implements RekeningService{

    @Autowired
    private RekeningRepo rekeningRepo;

//    @Autowired
//    private ProviderRepo providerRepo;
//    
//    @Autowired
//    private NasabahRepo nasabahRepo;

	@Override
	public List<Rekening> getAll() {
		// TODO Auto-generated method stub
        return this.rekeningRepo.findAll();
	}

	@Override
	public void save(Rekening rekening) {
		// TODO Auto-generated method stub
//		Provider provider = this.providerRepo.findByNama(rekening.getProvider().getNama());
//		Rekening rekening2 = new Rekening();
//        rekening2.setNasabah(rekening.getNasabah());
//        rekening2.setProvider(provider);
//        rekening2.setSaldo(1000000.0);
//        rekening2.setNoRekening(rekening.getNoRekening());
        this.rekeningRepo.save(rekening);

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
        this.rekeningRepo.deleteById(id);
	}


	@Override
	public Optional<Rekening> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.getRekeningById(id);
	}

	@Override
	public Rekening findByNoRek(String rekening) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findByNoRekening(rekening);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.rekeningRepo.findById(id);
	
	}

}
