package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.Provider;
import com.maybank.myweb.repository.ProviderRepo;

@Service
@Transactional
public class ProviderServiceImpl implements ProviderService {
	
	@Autowired
	private ProviderRepo providerRepo;

	@Override
	public List<Provider> getAll() {
		// TODO Auto-generated method stub
		return this.providerRepo.findAll();
	}

	@Override
	public void save(Provider provider) {
		// TODO Auto-generated method stub
		this.providerRepo.save(provider);
	}



	@Override
	public Optional<Provider> getProviderById(Long id) {
		// TODO Auto-generated method stub
		return this.providerRepo.findById(id);
	}
	
	
	@Override
	public Page<Provider> getAllPaginate(int pageNo, int pageSize, String sortField, String search) {
		// TODO Auto-generated method stub
		PageRequest paging = 
				PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		return this.providerRepo.findByNamaContaining(search, paging);
				}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.providerRepo.deleteById(id);
		
	}

//	@Override
//	public Provider getBankByNama(String nama) {
//		// TODO Auto-generated method stub
//		return this.providerRepo.findByNama(nama);
//	}
//	
	
	

}
