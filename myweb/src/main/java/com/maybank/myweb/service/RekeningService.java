package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.myweb.entity.Rekening;

public interface RekeningService {

    public List<Rekening> getAll();
    public void save(Rekening rekening);
    public void delete(Long id);
    public Optional<Rekening> getRekeningById(Long id);
	public Rekening findByNoRek(String noRek);
	public void getById(Long id);

}