package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.History;
import com.maybank.myweb.repository.HistoryRepo;

@Service
public class HistoryServiceImpl implements HistoryService {
	
	@Autowired
	private HistoryRepo historyRepo;

	@Override
	public List<History> getAll() {
		// TODO Auto-generated method stub
        return this.historyRepo.findAll();
	}

	@Override
	public void save(History history) {
		// TODO Auto-generated method stub
        this.historyRepo.save(history);

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
        this.historyRepo.deleteById(id);

	}

	@Override
	public Optional<History> getById(Long id) {
		// TODO Auto-generated method stub
        return this.historyRepo.findById(id);
	}
	
	

}
