package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;


import com.maybank.myweb.entity.Transaksi;

public interface TransaksiService {

	public List<Transaksi> getAll();
	public void save(Transaksi transaksi);
	public void delete(Long id);
	public Optional<Transaksi> getTransaksiById(Long id);
	
	public void saveHistory(Transaksi transaksi);

	
	
}
