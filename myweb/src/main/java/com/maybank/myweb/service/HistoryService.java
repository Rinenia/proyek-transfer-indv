package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.myweb.entity.History;
import com.maybank.myweb.entity.Nasabah;

public interface HistoryService {
	
	public List<History> getAll();
	public void save(History history);
	public void delete(Long id);
	public Optional<History> getById(Long id);

}
