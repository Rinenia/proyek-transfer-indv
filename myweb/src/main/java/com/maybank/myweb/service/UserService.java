package com.maybank.myweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.User;
import com.maybank.myweb.repository.RoleRepo;
import com.maybank.myweb.repository.UserRepo;



@Service
public class UserService {
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private RoleRepo roleRepo;
	
	public User findUserByUsername(String username){
		return this.userRepo.findByUsername(username);
	}
	

}
