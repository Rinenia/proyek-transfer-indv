package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.myweb.entity.Nasabah;
import com.maybank.myweb.entity.Rekening;

public interface NasabahService {
	
	public List<Nasabah> getAll();
	public void save(Nasabah nasabah);
	public void delete(Long id);
	public Page<Nasabah> getAllPaginate(int pageNo, int pageSize, String sortField, String search);
	public Optional<Nasabah> getNasabahById(Long id);
	public Optional<Nasabah> getNasabahByRek(Rekening rek);

}
