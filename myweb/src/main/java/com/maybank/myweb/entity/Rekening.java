package com.maybank.myweb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="rekening")
public class Rekening {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;
	

    private String noRekening;
//    @NotNull
//    @NotBlank
    private Double saldo;
    
    @ManyToOne(fetch = FetchType.EAGER.LAZY, cascade = CascadeType.ALL )
    @JoinColumn(name = "nasabah_id", referencedColumnName = "id")
    private Nasabah nasabah;
   
    @ManyToOne(fetch = FetchType.EAGER.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    private Provider provider;
    
    
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rekPengirims", cascade = CascadeType.ALL)
	private List<Transaksi> rekPengirim;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rekPenerimas", cascade = CascadeType.ALL)
	private List<Transaksi> rekPenerima;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Nasabah getNasabah() {
		return nasabah;
	}

	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public List<Transaksi> getRekPengirim() {
		return rekPengirim;
	}

	public void setRekPengirim(List<Transaksi> rekPengirim) {
		this.rekPengirim = rekPengirim;
	}

	public List<Transaksi> getRekPenerima() {
		return rekPenerima;
	}

	public void setRekPenerima(List<Transaksi> rekPenerima) {
		this.rekPenerima = rekPenerima;
	}
	
//    tranf one to many

	    
    
    
	

}
