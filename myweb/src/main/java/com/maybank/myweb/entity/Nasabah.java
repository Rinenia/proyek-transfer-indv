package com.maybank.myweb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="nasabah")
public class Nasabah {
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @NotBlank
//    @Column(unique = true)
    private String nik;
    @NotNull
    @NotBlank
    private String namaLengkap;
    
    private String tipeId;
    
    private String tanggalLahir;
    @NotNull
    @NotBlank
    private String email;
    @NotNull
    @NotBlank
    private String noContact;
//    @NotNull
//    @NotBlank
    @OneToMany(fetch= FetchType.LAZY, mappedBy = "nasabah", cascade = CascadeType.ALL)
    private List<Rekening> rekening;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNoContact() {
		return noContact;
	}
	public void setNoContact(String noContact) {
		this.noContact = noContact;
	}
	public List<Rekening> getRekening() {
		return rekening;
	}
	public void setRekening(List<Rekening> rekening) {
		this.rekening = rekening;
	}
	public String getTipeId() {
		return tipeId;
	}
	public void setTipeId(String tipeId) {
		this.tipeId = tipeId;
	}
    
//    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
//    private List<Provider> provider ;
    
    


}
