package com.maybank.myweb.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name="history")
public class History {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @NotBlank
    private Date tanggal;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "rekPengirims")
    private Transaksi rekPengirim;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "rekPenerimas")
    private Transaksi rekPenerima;
    private double amount;
    private double fee;
    
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getTanggal() {
		return tanggal;
	}
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public Object getRekPengirim() {
		return rekPengirim;
	}
	public void setRekPengirim(Object rekPengirim) {
		this.rekPengirim = (Transaksi) rekPengirim;
	}
	public Object getRekPenerima() {
		return rekPenerima;
	}
	public void setRekPenerima(Object rekPenerima) {
		this.rekPenerima = (Transaksi) rekPenerima;
	}


	
	
    
    


}
