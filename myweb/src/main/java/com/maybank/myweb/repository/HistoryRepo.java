package com.maybank.myweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.myweb.entity.History;

public interface HistoryRepo extends JpaRepository<History, Long>{

}
