package com.maybank.myweb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.myweb.entity.Provider;

public interface ProviderRepo extends JpaRepository<Provider, Long> {

	Page<Provider> findByNamaContaining(String search, PageRequest paging);
	
//	Provider findByNama(String nama);

}
