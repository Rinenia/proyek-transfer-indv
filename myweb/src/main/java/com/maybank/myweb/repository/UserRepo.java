package com.maybank.myweb.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.myweb.entity.User;


public interface UserRepo extends JpaRepository<User, Long> {

	User findByUsername(String username);
//	List<Role> findRoleByUser(User user);	

}

