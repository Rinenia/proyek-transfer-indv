package com.maybank.myweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.myweb.entity.Transaksi;

public interface TransaksiRepo extends JpaRepository<Transaksi, Long>{

}
