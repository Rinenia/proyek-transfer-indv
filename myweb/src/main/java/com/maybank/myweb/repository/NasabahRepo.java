package com.maybank.myweb.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.myweb.entity.Nasabah;
import com.maybank.myweb.entity.Rekening;

public interface NasabahRepo extends JpaRepository<Nasabah, Long> {

	Page<Nasabah> findByNamaLengkapContaining(String search, PageRequest paging);
	Optional<Nasabah> getNasabahByRekening(Rekening rek);

}
