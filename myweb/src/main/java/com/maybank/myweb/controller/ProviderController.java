package com.maybank.myweb.controller;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.myweb.entity.Provider;
import com.maybank.myweb.service.ProviderService;



@Controller
@RequestMapping("/provider")
public class ProviderController {

	
		@Autowired
		private ProviderService providerService;
		
		/*
		 * @GetMapping("/") public String indexHtml(Model model) { List<Provider>
		 * providers = this.providerService.getAll(); model.addAttribute("provideres",
		 * providers); return "provider"; }
		 */
	
		@GetMapping
		public String index(
				@RequestParam(value="pageNo", defaultValue = "0") int pageNo,
	            @RequestParam(value="pageSize", defaultValue = "10") int pageSize,
	            @RequestParam(value="sortField", defaultValue = "id") String sortField,
	            @RequestParam (value = "search", defaultValue = "") String search,
	            Model model) {
			Page<Provider> providers = this.providerService.getAllPaginate(pageNo, pageSize, sortField, search);
			model.addAttribute("page", providers);
			model.addAttribute("providerForm", new Provider());
			model.addAttribute("search", search);
				
			return "provider";
		}
		
		@PostMapping("/save")
	    public String save(@Valid @ModelAttribute("providerForm")
	            Provider provider, 
	            BindingResult result, //add binding result
	            RedirectAttributes redirectAttributes,
	            @RequestParam(value="page", defaultValue = "0") int pageNo,
	            @RequestParam(value="Size", defaultValue = "5") int pageSize,
	            @RequestParam(value="sortField", defaultValue = "id") String sortField,
	            @RequestParam (value = "search", defaultValue = "") String search,
	            Model model) {

	        if (result.hasErrors()) {
	        	Page<Provider> providers = this.providerService.getAllPaginate(pageNo, pageSize, sortField, search);
	            model.addAttribute("page",providers);
	            return "provider";}
	        

	        
	            this.providerService.save(provider);
	            redirectAttributes.addFlashAttribute("success","data inserted");
	            return "redirect:/provider";
		}
	        
		
		@GetMapping("/delete")
		public String delete(Provider provider, RedirectAttributes redirectAttributes) {
			this.providerService.delete(provider.getId());
			
			return "redirect:/provider";
		}
		
		@GetMapping("/edit")
		public String edit(@RequestParam("id")Long id, Model model) {
			Optional<Provider> providers = this.providerService.getProviderById(id);
			model.addAttribute("providerForm", providers);
			return "edit-provider";
		}
		
}