package com.maybank.myweb.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.myweb.entity.History;
import com.maybank.myweb.entity.Nasabah;
import com.maybank.myweb.entity.Provider;
import com.maybank.myweb.entity.Rekening;
import com.maybank.myweb.entity.Transaksi;
import com.maybank.myweb.service.HistoryService;
import com.maybank.myweb.service.NasabahService;
import com.maybank.myweb.service.ProviderService;
import com.maybank.myweb.service.RekeningService;
import com.maybank.myweb.service.TransaksiService;

@Controller
@RequestMapping("/transaksi")
public class TransaksiController {
	
	@Autowired
	private TransaksiService transaksiService;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private HistoryService historyService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private NasabahService nasabahService;
	
	
	double newSaldo;
	double saldo;
	
	@GetMapping
	public String indexHtml(Model model) {
		List<Provider> providers = this.providerService.getAll(); 
		List<Rekening> rekenings = this.rekeningService.getAll();
		List<Transaksi> transaksis = this.transaksiService.getAll();

		
		model.addAttribute("transaksis",transaksis);
		model.addAttribute("rekenings",rekenings);
		model.addAttribute("providers", providers);
		
		model.addAttribute("transaksiForm",new Transaksi());
		model.addAttribute("rekeningForm",new Rekening());
		model.addAttribute("providerForm",new Provider());
		model.addAttribute("nasabahForm",new Nasabah());
		return "transaksi";
	}
	
	@PostMapping("/save")
    public String save(
    		@ModelAttribute("transaksiForm")Transaksi transaksi,
    		RedirectAttributes redirectAttributes,
			Model model){
		List<Rekening> rekenings = this.rekeningService.getAll();
		List<Transaksi> transfers = this.transaksiService.getAll();
		List<Nasabah> nasabahs = this.nasabahService.getAll();
		Rekening rekeningPengirim = rekeningService.findByNoRek(transaksi.getRekPengirims().getNoRekening());
		Rekening rekeningPenerima = rekeningService.findByNoRek(transaksi.getRekPenerimas().getNoRekening());
		Long idPengirim = rekeningPengirim.getId();
		Long idPenerima = rekeningPenerima.getId();
		Optional<Rekening> pengirim = this.rekeningService.getRekeningById(idPengirim);
		Optional<Rekening> penerima = this.rekeningService.getRekeningById(idPenerima);
		

		
		Rekening rekPengirim = pengirim.get();
		Rekening rekPenerima = penerima.get();
		
		double amount = transaksi.getAmount();
		this.transaksiService.save(transaksi);

		
//		this.historyService.save(History history);
		return "redirect:/transaksi";
		}
	
	
    public boolean checkUser(Rekening rekening, List<Nasabah> user1){
		if(user1.contains(rekening)) {
    		return true;
		}
		else {
			return false;
		}
    		
	}
	
//    public boolean checkUser2(Rekening rekPenerima, List<Nasabah> user2){
//		if(user2.contains(rekPenerima)) {
//    		return true;
//		}
//		else {
//			return false;
//		}
//    		
//	}

	

}
