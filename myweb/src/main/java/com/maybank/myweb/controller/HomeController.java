package com.maybank.myweb.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maybank.myweb.entity.Nasabah;
import com.maybank.myweb.entity.Provider;
import com.maybank.myweb.entity.Rekening;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping
	public String indexHtml(Model model) {
                
        return "home";
	}

}
