package com.maybank.myweb.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.DaoAuthenticationConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.myweb.helper.UserAuthenticationSuccessHandler;
import com.maybank.myweb.service.CustomerDetailService;


@Configuration
@EnableWebSecurity
public class MyAuth {
	
	@Autowired
	private CustomerDetailService customerDetailService;
	
	@Autowired 
	UserAuthenticationSuccessHandler successHandler;
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

		authProvider.setUserDetailsService(customerDetailService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());; 
		
		return authProvider;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder(){
		return new BCryptPasswordEncoder();
		
	}
	
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.authorizeRequests().antMatchers("/provider").hasAnyAuthority("admin");
        httpSecurity.authorizeRequests().antMatchers("/nasabah").hasAnyAuthority("cs");
        httpSecurity.authorizeRequests().antMatchers("/transaksi").hasAnyAuthority("operator");
        httpSecurity.authorizeRequests().anyRequest().authenticated();
        httpSecurity.authorizeRequests().and().formLogin().successHandler(successHandler);
        return httpSecurity.build();
    }
	
	
//	@Bean
//	public UserDetailsService userDetailsService(BCryptPasswordEncoder byBCryptPasswordEncoder) {
//		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//		manager.createUser(User.withUsername("cathrine").
//				password(byBCryptPasswordEncoder.encode("123456")).
//				roles("admin").
//				build());
//		
//		manager.createUser(User.withUsername("boni").
//				password(byBCryptPasswordEncoder.encode("123456")).
//				roles("user").
//				build());
//	return manager;
//	}

	
}
