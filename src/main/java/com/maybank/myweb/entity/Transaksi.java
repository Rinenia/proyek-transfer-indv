package com.maybank.myweb.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaksi")
public class Transaksi {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private Long id;

	
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Rekening rekPengirims;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Rekening rekPenerimas;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private History history;
    
    
    private Date tanggal;
    private double amount;
    private double fee;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Rekening getRekPengirims() {
		return rekPengirims;
	}
	public void setRekPengirims(Rekening rekPengirims) {
		this.rekPengirims = rekPengirims;
	}
	public Rekening getRekPenerimas() {
		return rekPenerimas;
	}
	public void setRekPenerimas(Rekening rekPenerimas) {
		this.rekPenerimas = rekPenerimas;
	}
	public History getHistory() {
		return history;
	}
	public void setHistory(History history) {
		this.history = history;
	}
	public Date getTanggal() {
		return tanggal;
	}
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	
    
    
	
    
}
