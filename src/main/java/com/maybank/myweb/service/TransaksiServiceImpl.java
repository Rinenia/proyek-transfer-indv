package com.maybank.myweb.service;

import java.sql.Date;
import java.util.List;

import java.time.LocalDate;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.History;
import com.maybank.myweb.entity.Rekening;
import com.maybank.myweb.entity.Transaksi;
import com.maybank.myweb.repository.HistoryRepo;
import com.maybank.myweb.repository.RekeningRepo;
import com.maybank.myweb.repository.TransaksiRepo;

@Service
@Transactional
public class TransaksiServiceImpl implements TransaksiService {
	
	@Autowired
	private TransaksiRepo transaksiRepo;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Autowired
	private HistoryRepo historyRepo;

	@Override
	public List<Transaksi> getAll() {
		// TODO Auto-generated method stub
        return this.transaksiRepo.findAll();
	}

	@Override
	public void save(Transaksi transaksi) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Rekening rekeningPengirim = rekeningService.findByNoRek(transaksi.getRekPengirims().getNoRekening());
		Rekening rekeningPenerima = rekeningService.findByNoRek(transaksi.getRekPenerimas().getNoRekening());
		
		Long idPengirim = rekeningPengirim.getId();
		Long idPenerima = rekeningPenerima.getId();
		
		Optional<Rekening> pengirim = this.rekeningService.getRekeningById(idPengirim);
		Optional<Rekening> penerima = this.rekeningService.getRekeningById(idPenerima);
		
		Rekening rekPengirim = pengirim.get();
		Rekening rekPenerima = penerima.get();
		
		String bankAsal = rekPengirim.getProvider().getNama();
		String bankTuju = rekPenerima.getProvider().getNama();
		
		Transaksi transaksii = new Transaksi();
		transaksii.setTanggal(Date.valueOf(LocalDate.now()));
		transaksii.setAmount(transaksi.getAmount());
		
//		buat history
		History history = new History();
		history.setTanggal(Date.valueOf(LocalDate.now()));
		history.setAmount(transaksi.getAmount());
//		sampe sini
		
		if(bankAsal.equals(bankTuju)) {
			//transfer.setFee(0);
			rekPengirim.setSaldo(rekPengirim.getSaldo()-(transaksii.getAmount()));
			transaksii.setRekPengirims(rekPengirim);
			transaksii.setRekPenerimas(rekPenerima);	
			rekPenerima.setSaldo(rekPenerima.getSaldo() + (transaksii.getAmount()));
			this.rekeningRepo.save(rekPengirim);
			this.rekeningRepo.save(rekPenerima);
			this.transaksiRepo.save(transaksii);

		}else if(bankAsal != bankTuju) {
			double fee = 6500;
			rekPengirim.setSaldo(rekPengirim.getSaldo()-(transaksii.getAmount())-fee);
			transaksii.setFee(6500);
			transaksii.setRekPengirims(rekPengirim);
			transaksii.setRekPenerimas(rekPenerima);
			rekPenerima.setSaldo(rekPenerima.getSaldo() + (transaksii.getAmount()));
			this.rekeningRepo.save(rekPengirim);
			this.rekeningRepo.save(rekPenerima);
			this.transaksiRepo.save(transaksii);
	
		}
		
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
        this.transaksiRepo.deleteById(id);

	}

	@Override
	public Optional<Transaksi> getTransaksiById(Long id) {
		// TODO Auto-generated method stub
        return this.transaksiRepo.findById(id);
	}

	@Override
	public void saveHistory(Transaksi transaksi) {
		// TODO Auto-generated method stub
		
		Rekening rekeningPengirim = rekeningService.findByNoRek(transaksi.getRekPengirims().getNoRekening());
		Rekening rekeningPenerima = rekeningService.findByNoRek(transaksi.getRekPenerimas().getNoRekening());
		
		Long idPengirim = rekeningPengirim.getId();
		Long idPenerima = rekeningPenerima.getId();
		
		Optional<Rekening> pengirim = this.rekeningService.getRekeningById(idPengirim);
		Optional<Rekening> penerima = this.rekeningService.getRekeningById(idPenerima);
		
		Rekening rekPengirim = pengirim.get();
		Rekening rekPenerima = penerima.get();
		
		String bankAsal = rekPengirim.getProvider().getNama();
		String bankTuju = rekPenerima.getProvider().getNama();
		
		Transaksi transaksii = new Transaksi();
		transaksii.setTanggal(Date.valueOf(LocalDate.now()));
		transaksii.setAmount(transaksi.getAmount());
		
//		buat history
		History history = new History();
		history.setTanggal(Date.valueOf(LocalDate.now()));
		history.setAmount(transaksi.getAmount());
//		sampe sini
		
		if(bankAsal.equals(bankTuju)) {
			//transfer.setFee(0);
			history.setRekPenerima(rekPengirim);
			history.setRekPengirim(rekPenerima);
			history.setAmount(rekPengirim.getSaldo());
			this.historyRepo.save(history);
		}else if(bankAsal != bankTuju) {
			
//			History
			history.setRekPenerima(transaksii.getRekPenerimas());
			history.setRekPengirim(transaksii.getRekPengirims());
			history.setAmount(transaksii.getAmount());
			this.historyRepo.save(history);
			
			
		}
	}
	
	

}
