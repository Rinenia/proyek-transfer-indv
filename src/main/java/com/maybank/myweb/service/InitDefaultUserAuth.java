package com.maybank.myweb.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.Role;
import com.maybank.myweb.entity.User;
import com.maybank.myweb.repository.RoleRepo;
import com.maybank.myweb.repository.UserRepo;



@Service
@Transactional
public class InitDefaultUserAuth {
	
	@Autowired
	private RoleRepo roleRepo;
	
	@Autowired
	private UserRepo userRepo;
	
	@PostConstruct
	public void index() {
		
		Role roleCs = new Role();
		Role roleAdmin = new Role();
		Role roleOperator = new Role();
		
		roleCs.setRole("cs");
		roleAdmin.setRole("admin");
		roleOperator.setRole("operator");
		
		this.roleRepo.save(roleCs);
		this.roleRepo.save(roleAdmin);
		this.roleRepo.save(roleOperator);
		
//		create user
		List<Role> listRole = new ArrayList<>();
		List<Role> userListRole = new ArrayList<>();
		List<Role> operatorListRole = new ArrayList<>();

		listRole.add(roleAdmin);
		userListRole.add(roleCs);
		operatorListRole.add(roleOperator);
		
		User userAdmin = new User();
		userAdmin.setUsername("admin");
		userAdmin.setEmail("maybank@gmail.com");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userAdmin.setRoles(listRole);
		
		User userCs = new User();
		userCs.setUsername("customerservice");
		userCs.setEmail("user@gmail.com");
		userCs.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userCs.setRoles(userListRole);
		
		User userOperator = new User();
		userOperator.setUsername("operator");
		userOperator.setEmail("operator@gmail.com");
		userOperator.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userOperator.setRoles(operatorListRole);
		
		
		this.userRepo.save(userAdmin);
		this.userRepo.save(userCs);
		this.userRepo.save(userOperator);
		
	}

}
