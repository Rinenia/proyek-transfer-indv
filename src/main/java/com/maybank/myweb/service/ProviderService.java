package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.myweb.entity.Provider;

public interface ProviderService {
	
	public List<Provider> getAll();
	public void save(Provider provider);
	public void delete(Long id);
//	public Provider getBankByNama(String nama);
	public Page<Provider> getAllPaginate(int pageNo, int pageSize, String sortField, String search);
	public Optional<Provider> getProviderById(Long id);

}
