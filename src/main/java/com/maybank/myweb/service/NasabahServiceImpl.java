package com.maybank.myweb.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.maybank.myweb.entity.Nasabah;
import com.maybank.myweb.entity.Rekening;
import com.maybank.myweb.repository.NasabahRepo;

@Service
@Transactional
public class NasabahServiceImpl implements NasabahService{
	
	@Autowired
	private NasabahRepo nasabahRepo;

	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findAll();
	}

	@Override
	public void save(Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.nasabahRepo.save(nasabah);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.nasabahRepo.deleteById(id);
	
	}

	@Override
	public Page<Nasabah> getAllPaginate(int pageNo, int pageSize, String sortField, String search) {
		// TODO Auto-generated method stub
		PageRequest paging = 
				PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		return this.nasabahRepo.findByNamaLengkapContaining(search, paging);	}

	@Override
	public Optional<Nasabah> getNasabahById(Long id) {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findById(id);
	}

	@Override
	public Optional<Nasabah> getNasabahByRek(Rekening rek) {
		// TODO Auto-generated method stub
		return this.nasabahRepo.getNasabahByRekening(rek);
	}
	
	

}
