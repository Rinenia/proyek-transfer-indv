package com.maybank.myweb.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.myweb.entity.Rekening;

public interface RekeningRepo extends JpaRepository<Rekening, Long> {
	
	Rekening findByNoRekening(String rekening);

	Optional<Rekening> getRekeningById(Long id); 

}
