package com.maybank.myweb.controller;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.maybank.myweb.entity.Nasabah;
import com.maybank.myweb.entity.Provider;
import com.maybank.myweb.entity.Rekening;
import com.maybank.myweb.service.NasabahService;
import com.maybank.myweb.service.ProviderService;
import com.maybank.myweb.service.RekeningService;

@Controller
@RequestMapping("/nasabah")
public class NasabahController {

	@Autowired
	private NasabahService nasabahService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private RekeningService rekeningService;
	
	
	
	
	@GetMapping
	public String indexHtml(Model model) {
		 List<Provider> providers = this.providerService.getAll(); 
	     List<Rekening> rekenings = this.rekeningService.getAll(); 

	     model.addAttribute("rekenings",rekenings);
	     model.addAttribute("providers", providers);

	     model.addAttribute("rekeningForm",new Rekening());
	     model.addAttribute("providerForm",new Provider());
	     model.addAttribute("nasabahForm",new Nasabah());
        
        
        return "nasabah";
	}

	
	/*
	 * public String index(
	 * 
	 * @RequestParam(value="pageNo", defaultValue = "0") int pageNo,
	 * 
	 * @RequestParam(value="pageSize", defaultValue = "10") int pageSize,
	 * 
	 * @RequestParam(value="sortField", defaultValue = "id") String sortField,
	 * 
	 * @RequestParam (value = "search", defaultValue = "") String search, Model
	 * model) { Page<Nasabah> nasabahs = this.nasabahService.getAllPaginate(pageNo,
	 * pageSize, sortField, search); model.addAttribute("pages", nasabahs);
	 * model.addAttribute("nasabahForm", new Nasabah());
	 * 
	 * 
	 * 
	 * model.addAttribute("search", search);
	 * 
	 * return "nasabah"; }
	 */
	@PostMapping("/save")
    public String save(@ModelAttribute("nasabahForm")Nasabah nasabah,
    		@ModelAttribute("rekeningForm")Rekening rekening,
    		@ModelAttribute("providerForm")Provider provider,
            BindingResult result, //add binding result
//            RedirectAttributes redirectAttributes,
//            @RequestParam(value="page", defaultValue = "0") int pageNo,
//            @RequestParam(value="Size", defaultValue = "5") int pageSize,
//            @RequestParam(value="sortField", defaultValue = "id") String sortField,
//            @RequestParam (value = "search", defaultValue = "") String search,
            Model model) {
		if(result.hasErrors()) {
			model.addAttribute("rekenings", rekening);
			return "nasabah";
		}
		else {
		    rekening.setSaldo(1000000.0);
		    rekening.setNasabah(nasabah);
		    rekening.setProvider(provider);
		
		    this.rekeningService.save(rekening);
		    this.nasabahService.save(nasabah);
		    this.providerService.save(provider);

		    return "redirect:/nasabah";
			
		}
		


	}
        
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id")Long id) {
		Optional<Rekening> rekenings = this.rekeningService.getRekeningById(id);
		if(rekenings.isPresent()) {
			this.rekeningService.delete(id);
		}
		return"redirect:/nasabah";
	}
	
	@GetMapping("/edit")
	public String edit(@RequestParam("id")Long id, Model model) {
//		Optional<Rekening> rekenings = this.rekeningService.getRekeningById(id);
//		Optional<Nasabah> nasabahs = this.nasabahService.getNasabahByRekening(rekenings);
//		model.addAttribute("nasabahForm", rekenings);
		return "edit-nasabah";
	}
}
